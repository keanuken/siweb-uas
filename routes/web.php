<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ImageUploadController;
use App\Http\Controllers\KaryaUserController;
use App\Http\Controllers\KomentarSenimanController;
use App\Http\Controllers\KomentarUserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;

Route::get('/', function () {
    return view('content.home');
});

Route::get('/user', function () {
    return view('content.user.index');
});

Route::get('/seniman', function () {
    return view('content.seniman.index');
});

Route::get('/seniman/input', function () {
    return view('content.seniman.input-karya');
});

Route::get('/login', function () {
    return view('content.login');
});

Route::get('/register', function () {
    return view('content.register');
})->middleware('guest');

Route::get('/user/komentar', function () {
    return view('content.user.input-komentar');
});

Route::get('/seniman/komentar/karya', function () {
    return view('content.seniman.input-komentar');
});

// login
Route::post('/post-register', [RegisterController::class, 'store']);
Route::post('/post-login', [AuthController::class, 'postLogin']);
Route::get('/logout', [AuthController::class, 'logout']);

//karya seniman
Route::get('/seniman', [ImageUploadController::class, 'index']);
Route::get('/seniman/input', [ImageUploadController::class, 'showKategori']);
Route::post('/seniman/post-karya', [ImageUploadController::class, 'store']);
Route::get('/seniman/edit/karya/{id}', [ImageUploadController::class, 'edit']); //edit karya
Route::delete('/seniman/delete/karya/{id}', [ImageUploadController::class, 'delete']); //delete karya
Route::put('/seniman/{id}', [ImageUploadController::class, 'update']); //update karya

// karya user
Route::get('/user', [KaryaUserController::class, 'index']);

//crud komentar user
Route::get('/user/edit-komentar/{id}', [KomentarUserController::class, 'edit']); //select data
Route::put('/user/list-komentar-user/{id}', [KomentarUserController::class, 'update']); //update data
Route::delete('user/delete-komentar/{id}', [KomentarUserController::class, 'delete']);
//input komentar user
Route::get('/user/input-komentar/{id}', [KomentarUserController::class, 'create']); //create data
Route::post('/user/input-komentar', [KomentarUserController::class, 'store']); //simpan data
Route::get('/user/list-komentar-user', [KomentarUserController::class, 'index']); //tampilkan data

//crud komentar seniman
Route::get('/seniman/edit-komentar/{id}', [KomentarSenimanController::class, 'edit']); //select data
Route::put('/seniman/list-komentar-seniman/{id}', [KomentarSenimanController::class, 'update']); //update data
Route::delete('seniman/delete-komentar/{id}', [KomentarSenimanController::class, 'delete']);
//input komentar seniman
Route::get('/seniman/input-komentar/{id}', [KomentarSenimanController::class, 'create']); //create data
Route::post('/seniman/input-komentar', [KomentarSenimanController::class, 'store']); //simpan data
Route::get('/seniman/list-komentar-seniman', [KomentarSenimanController::class, 'index']);//tampilkan data