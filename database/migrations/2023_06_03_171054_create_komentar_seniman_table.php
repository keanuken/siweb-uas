<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(): void
    {
        Schema::create('komentar_seniman', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->String('id_komentar');
            $table->String('id_user');
            $table->String('id_karya');
            $table->String('komentar');
            $table->String('tanggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('komentar_seniman');
    }
};
