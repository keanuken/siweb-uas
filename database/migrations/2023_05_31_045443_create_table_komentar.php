<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_komentar', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('id_komentar',255);
            $table->string('id_user',255);
            $table->string('id_karya',255);
            $table->string('komentar',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_komentar');
    }
};
