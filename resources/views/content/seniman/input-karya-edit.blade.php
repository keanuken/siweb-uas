@extends('layout.input-karya')
@section('content')
    @include('component.navSeniman')
    <main class="bg mt-5 pb-5">
        <form action="{{ url('seniman', $table_karya->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <section class="d-flex flex-row container gap-3 justify-content-center">
                <div class="img m-0 p-0" style="width:500px; height:800px;">
                    <img id="preview-image-before-upload" class="pre-img object-fit-cover overflow-hidden"
                        style="width: 500px; height: 800px;" src="{{ asset('assets/' . $table_karya->image) }}">
                </div>

                <div class="container bg-black ms-auto" style="width:800px; height:800px;">
                    <form action="post-karya" method="POST" enctype="multipart/form-data" id="upload-image">
                        <div class="container-fluid pt-5">
                            <div class="input-form text-white pt-5 px-4">
                                <div class="form-title mb-5">
                                    <h1>Post your Amazing Art</h1>
                                    <p>"Art has no rules"</p>
                                </div>
                                <div class="field-group d-none">
                                    <input type="text" id="id_seniman" name="id_seniman" class="form-control"
                                        value="{{ Auth::user()->id }}">
                                </div>
                                <div class="field-group">
                                    <label class="form-label my-3">Nama Karya</label>
                                    <input type="text" id="nama_karya" name="nama_karya" class="form-control"
                                        value="{{ $table_karya->nama_karya }}">
                                </div>
                                <div class="field-group
                                        my-3">
                                    <label class="form-label">Deskripsi Karya</label>
                                    <textarea type="text" id="deskripsi" name="deskripsi" class="form-control"
                                        placeholder="{{ $table_karya->deskripsi }}"></textarea>
                                </div>
                                <div class="field-group my-3 dropdown">
                                    <label class="form-label" for="wujud">Wujud</label>
                                    <select name="wujud" class="form-control" required>
                                        <option nama="dropdown" value="{{ $table_karya->wujud }}">{{ $table_karya->wujud }}
                                        </option>

                                    </select>
                                </div>
                                <div class="my-3">
                                    <div class="form-group">
                                        <label class="form-label">Upload</label>
                                        <input type="file" class="form-control" name="image" id="image">
                                        @error('image')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="my-5 d-flex justify-content-center align-items-center">
                                    <button type="submit" class="btn btn-primary bg-white text-black border-0"
                                        style="width: 200px; border-radius: 0;" id="submit">Save your edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </form>
    </main>
    @include('component.footer')
@endsection
