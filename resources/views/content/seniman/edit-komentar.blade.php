@extends('layout.seniman')

@section('content')

@include('component.navSeniman')
<div class="container-fluid" style="background-image : url('/assets/sec-bg.jpg');">
    <div class="row">
        <div class="col-sm-10 col-md-7 col-lg-9 mx-auto">
          <div class="card border-0 shadow rounded-3 my-5">
            <div class="card-body p-4 p-sm-5">
              <h1 class="card-title text mb-8">Give your Comment</h1>
              <p class="card-title text mb-5"><i>on something amazing</i></p>
    <form action="{{ url('/seniman/list-komentar-seniman', $komentar_seniman->id)}}" method="POST"> 
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label class="form-label" for="name">ID Komentar</label>
            <input type="text" id="id_komentar" type="text" class="form-control" name="id_komentar"  value="{{ $komentar_seniman->id_komentar }}">>
        </div>
        <div class="form-group">
            <label class="form-label" for="name">ID User</label>
            <input type="text" id="id_user" type="text" class="form-control" name="id_user" value="{{ $komentar_seniman->id_user }}">
        </div>
        <br>
        <div class="form-group">
            <label class="form-label" for="name">ID Karya</label>
            <input type="text" id="id_karya" type="text" class="form-control" name="id_karya" value="{{ $komentar_seniman->id_karya}}">
        </div>
        <br>
        <div class="form-group">
            <label class="form-label" for="name">Komentar</label>
            <input type="text" id="komentar" type="text" class="form-control" name="komentar" value="{{ $komentar_seniman->komentar }}" required autofocus>
        </div>
        <br>
        <div class="form-group">
            <label class="form-label" for="name">Tanggal Komentar</label>
            <input type="date" id="tanggal" type="date" class="form-control" name="tanggal" value="{{ $komentar_seniman->tanggal }}" required autofocus>
        </div>
        <br>
        <button class="btn btn-primary" style="background-color: #000000" type="submit">Posting</button>
    </form>
            </div>
          </div>
        </div>
    </div>
</div>
@include('component.footer')
    
    @endsection