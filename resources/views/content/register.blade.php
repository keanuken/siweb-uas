@extends('layout.register')
@section('content')
    {{-- @include('component.navbar') --}}
    <div class="container-fluid bg">
        <div class="form">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="login-form">
                        <h1>Visualize</h1>
                        <p>Register account in Visualize.</p>
                        <form action="post-register" method="POST">
                            {{ csrf_field() }}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }} <br />
                                    @endforeach
                                </div>
                            @endif
                            <div class="field-group">
                                <input type="name" name="name" class="input-field" aria-describedby="name"
                                    placeholder="Name" autocomplete="off">
                            </div>
                            <div class="field-group my-5">
                                <input type="email" name="email" class="input-field" aria-describedby="email"
                                    placeholder="Email address">
                            </div>
                            <div class="field-group my-5">
                                <input type="password" name="password" class="input-field" aria-describedby="password"
                                    placeholder="Password">
                            </div>
                            <div class="dropdown my-5">
                                <label for="title-level" class="fw-bold fs-5">Level</label>
                                <select name="level" class="form-control mt-3" required>
                                    <option nama="dropdown" value="user">User</option>
                                    <option nama="dropdown" value="seniman">Seniman</option>
                                </select>
                            </div>
                            <br>
                            <br>
                            <div class="form-button d-flex align-items-center">
                                <button type="submit" class="btn btn-submit">Register</button>
                                <a href="/login" class="ms-auto me-4 text-secondary">Already have an account?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('component.footer')
@endsection
