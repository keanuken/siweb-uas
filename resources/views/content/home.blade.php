@extends('layout.home')
@section('content')
    {{-- masih proto, nanti bakal diganti pake routing dari controller kalau udah ketemu. --}}
    @include('component.navbar')
    <main class="mb-5">
        <div class="container bg-black py-5">
            <div class="container px-5">
                <h1 class="text-white">Selamat Datang</h1>
                <h1 class="text-white">di galeri digital Visualize.</h1>
            </div>
        </div>
        <div class="container-fluid galeries d-flex justify-content-center mt-4">
            <div class="deskripsi-left bg-primary text-white pt-5">
                <h1>Yang Berusaha Tumbuh</h1>
                <p>
                    Dede Eri Supria (1956)
                </p>
            </div>
            <img src="assets/image1.png" alt="image.png" class="galeri-img" />
        </div>
        <div class="container-fluid galeries d-flex justify-content-center mt-4">
            <img src="assets/image2.png" alt="image.png" class="galeri-img" />
            <div class="deskripsi-right bg-danger text-white pt-5 text-end pe-4">
                <h1>Diskusi di Sanggar Bumi Tarung</h1>
                <p>
                    Sanggar Bumi Tarung (2008)
                </p>
            </div>
        </div>
        <div class="container-fluid galeries d-flex justify-content-center mt-4">
            <div class="deskripsi-left bg-warning text-white pt-5">
                <h1>Dunia Anjing</h1>
                <p>
                    Agus Djaya (1965)
                </p>
            </div>
            <img src="assets/image3.png" alt="image.png" class="galeri-img" />
        </div>
        <div class="container-fluid galeries d-flex justify-content-center mt-4">
            <img src="assets/image4.png" alt="image.png" class="galeri-img" />
            <div class="deskripsi-right pt-5 bg-success text-white text-end pe-4">
                <h1>Upacara Bali</h1>
                <p>
                    Trisno Sumardjo (1953)
                </p>
            </div>
        </div>
    </main>
    @include('component.footer')
@endsection
