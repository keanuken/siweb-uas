@extends('layout.seniman')
@section('content')
    @include('component.navUser')
    <main class="bg-dark text-white">
        <div class="container">
            <?php
            $userController = new App\Http\Controllers\UserController();
            ?>
            <h1 class="greets py-5">Hi 👋🏻 , <span class="fw-bold">{{ $userController->showUserName() }}</span>
                <br>Selamat
                datang
                di
                Visualize
            </h1>
            <section class="karya py-5">
                <h1 class="fs-1 py-3 fw-bold text-uppercase ms-3">Karya - Karya</h1>
                <div class="list-card">
                    @foreach ($table_karya as $kar)
                        <div class="course_card text-black">
                            <div class="course_card_img">
                                <img src="{{ asset('assets/' . $kar->image) }}">
                            </div>
                            <div class="course_card_content mb-0 pb-0">
                                <h3 class="title">{{ $kar->nama_karya }} </h3>
                                <p class="text-secondary">{{ $kar->wujud }}</p>
                                <p class="description">{{ $kar->deskripsi }}</p>
                            </div>
                            <div class="course_card_footer pt-0 my-3">
                                <form action="{{ url('/seniman/delete/karya', $kar->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <a href="{{ '/user/input-komentar/' . $kar->id }}" class="btn-komentar nav-item">Tambahkan
                                        Komentar</a>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        </div>
    </main>
    @include('component.footer')
@endsection
