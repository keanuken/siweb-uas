@extends('layout.user')

@section('content')

@include('component.navUser')
<div class="container-fluid" style="background-image : url('/assets/sec-bg.jpg');">
    <div class="row">
        <div class="col-sm-10 col-md-7 col-lg-9 mx-auto">
          <div class="card border-0 shadow rounded-3 my-5">
            <div class="card-body p-4 p-sm-5">
              <h1 class="card-title text mb-8">Give your Comment</h1>
              <p class="card-title text mb-5"><i>on something amazing</i></p>
    <form action="{{ url('/user/list-komentar-user', $komentar_user->id)}}" method="POST"> 
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group d-none">
            <label class="form-label" for="name">ID User</label>
            <input type="text" id="id_user" type="text" class="form-control" name="id_user" value="{{ $komentar_user->id_user }}">
        </div>
        <br>
        <div class="form-group d-none">
            <label class="form-label" for="name">ID Karya</label>
            <input type="text" id="id_karya" type="text" class="form-control" name="id_karya" value="{{ $komentar_user->id_karya}}">
        </div>
        <br>
        <div class="form-group">
            <label class="form-label" for="name">Komentar</label>
            <input type="text" id="komentar" type="text" class="form-control" name="komentar" value="{{ $komentar_user->komentar }}" required autofocus>
        </div>
        <br>
        <br>
        <button class="btn btn-primary" style="background-color: #000000" type="submit">Posting</button>
    </form>
            </div>
          </div>
        </div>
    </div>
</div>
@include('component.footer')
    
    @endsection