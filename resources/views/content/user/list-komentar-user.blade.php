@extends('layout.user')

@section('content')

    @include('component.navUser')
    <div class="container-fluid" style="background-image : url('/assets/sec-bg.jpg');">
        <div class="row">
            <div class="col-sm-10 col-md-7 col-lg-9 mx-auto">
                <div class="card border-0 shadow rounded-3 my-5">
                    <div class="card-body p-4 p-sm-5">
                        <h1 class="card-title text mb-8">List Comment User</h1>
                        <td><a href="/user/input-komentar" class="btn btn-primary">Add Comment</a></td>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Id Komentar</th>
                                        <th scope="col">Id User</th>
                                        <th scope="col">Id Karya</th>
                                        <th scope="col">Komentar</th>
                                        <th scope="col">Tanggal</th>
                                        <th scope = "col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($komentar_user as $kmt)
                                        <tr>
                                            <td>{{$kmt->id}}</td>
                                            <td>{{$kmt->id_user}}</td>
                                            <td>{{$kmt->id_karya}}</td>
                                            <td>{{$kmt->komentar}}</td>
                                            <td>{{$kmt->created_at}}</td>
                                            <td>
                                                <form action="{{ url('user/delete-komentar', $kmt->id)}}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <a href="{{'edit-komentar/'.$kmt->id}}" class="btn btn-warning">Edit</a>
                                                    <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure?')">
                                                        Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('component.footer')

@endsection
