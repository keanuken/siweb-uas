@extends('layout.login')
@section('content')
    {{-- @include('component.navbar') --}}
    <div class="container-fluid bg">
        <div class="form">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="login-form">
                        <h1>Visualize</h1>
                        <p>Log in to your account with your email <br> and password.</p>
                        <form action="post-login" method="POST">
                            {{ csrf_field() }}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }} <br />
                                    @endforeach
                                </div>
                            @endif
                            <div class="field-group">
                                <input type="email" id="email" name="email" class="input-field"
                                    aria-describedby="email" placeholder="Email address" autocomplete="off">
                            </div>
                            <div class="field-group my-5">
                                <input type="password" id="password" name="password" class="input-field"
                                    aria-describedby="password" placeholder="Password">
                            </div>
                            <div class="form-button d-flex align-items-center">
                                <button type="submit" class="btn btn-submit">Log In</button>
                                <a href="#" class="ms-auto me-4 text-secondary">Forgot your password?</a>
                            </div>
                            <div class="form-footer d-flex flex-column mt-5">
                                <p href="/register" class="text-black fs-6">Not a member?<a href="register"
                                        class="text-black">
                                        Register here.</a></p>
                                <a href="/register" class="text-decoration-none text-black fs-5 join mt-4">Join Today<i
                                        class="fa-solid fa-arrow-right ms-3 fs-5"></i></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('component.footer')
@endsection
