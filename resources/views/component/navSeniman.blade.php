<nav class="navbar navbar-expand-lg bg-white py-5 sticky-top">
    <div class="container">
        <div class="d-flex flex-column">
            <a class="navbar-brand fw-bold" href="/seniman">Visualize</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav gap-5">
                    <li class="nav-item">
                        <a class="nav-link active fw-bold" href="/seniman">Karya</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active fw-bold" href="/seniman/input">Tambah Karya</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active fw-bold" href="/seniman/list-komentar-seniman">Komentar</a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav-right gap-5">
            <a class="btn right-menu" href="/logout">Logout</a>
        </ul>
    </div>
</nav>
