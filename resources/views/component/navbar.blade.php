<nav class="navbar navbar-expand-lg bg-white py-5 sticky-top">
    <div class="container">
        <div class="d-flex flex-column">
            <a class="navbar-brand fw-bold" href="/">Visualize</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                {{-- <ul class="navbar-nav gap-5">
                    <li class="nav-item">
                        <a class="nav-link active fw-bold" href="/">Karya</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active fw-bold" href="#">Kategori</a>
                    </li>
                </ul> --}}
            </div>
        </div>
        <ul class="nav-right gap-5">
            <a class="btn right-menu" href="/register">Register</a>
            <a class="btn right-menu" href="/login">Login</a>
        </ul>
        {{-- <div class="container d-flex mb-2 mb-lg-0 bg-primary w-auto gap-5 navbar-right">
            <a class="btn btn-register" href="#">Register</a>
            <a class="btn btn-login" href="#">Login</a>
        </div> --}}
    </div>
</nav>
