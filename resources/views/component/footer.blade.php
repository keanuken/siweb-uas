<!-- Footer -->
<footer class="text-center text-lg-start text-white" style="background-color: #000000">
    <!-- Grid container -->
    <div class="container p-4 pb-0">
        <!-- Section: Links -->
        <section class="">
            <!--Grid row-->
            <div class="row">
                <!-- Grid column -->
                <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">
                        VISUALIZE
                    </h6>
                    <p>
                        Visualize adalah website yang menampilkan karya
                        karya seniman berbakat agar penikmat seni dapat
                        melihat secara online
                    </p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none" />

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Karya</h6>
                    <p>
                        <a class="text-white">At the Cockfight</a>
                    </p>
                    <p>
                        <a class="text-white">Fantasy Landscape</a>
                    </p>
                    <p>
                        <a class="text-white">Dua Sahabat</a>
                    </p>
                    <p>
                        <a class="text-white">The Man From Bantul</a>
                    </p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none" />

                <!-- Grid column -->
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">
                        Seniman
                    </h6>
                    <p>
                        <a class="text-white">Affandi Koesoema</a>
                    </p>
                    <p>
                        <a class="text-white">Kartono Yudhokusumo</a>
                    </p>
                    <p>
                        <a class="text-white">Achmad Sadali</a>
                    </p>
                    <p>
                        <a class="text-white">I Nyoman Masriadi</a>
                    </p>
                </div>

                <!-- Grid column -->
                <hr class="w-100 clearfix d-md-none" />

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                    <p><i class="fas fa-home mr-3"></i> Jalan Taman Srigunting nomor 5-6, Kecamatan Semarang Utara, Kota
                        Semarang, Jawa Tengah</p>
                    <p><i class="fas fa-envelope mr-3"></i> visualize@gmail.com</p>
                    <p><i class="fas fa-phone mr-3"></i>+62243552099</p>
                    <p><i class="fas fa-print mr-3"></i>022 374823</p>
                </div>
                <!-- Grid column -->
            </div>
            <!--Grid row-->
        </section>
        <!-- Section: Links -->

        <hr class="my-3">

        <!-- Section: Copyright -->
        <section class="p-3 pt-0">
            <div class="row d-flex align-items-center">
                <!-- Grid column -->
                <div class="col-md-7 col-lg-8 text-center text-md-start">
                    <!-- Copyright -->
                    <div class="p-3">
                        © 2023 Copyright:
                        <a class="text-white" href="https://mdbootstrap.com/">Visualize.com</a>
                    </div>
                    <!-- Copyright -->
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-5 col-lg-4 ml-lg-0 text-center text-md-end">
                    <!-- Facebook -->
                    {{-- <a class="text-white">ABOUT US</a> --}}
                    <td>"Melukis hanyalah cara lain untuk membuat buku harian." - Pablo Picasso</td>
                </div>
                <!-- Grid column -->
            </div>
        </section>
        <!-- Section: Copyright -->
    </div>
    <!-- Grid container -->
</footer>
<!-- Footer -->
</div>
