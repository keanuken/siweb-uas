<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KomentarSeniman;
use App\Models\table_karya;

class KomentarSenimanController extends Controller
{
    //
    public function index()
    {
        $data['komentar_seniman'] = KomentarSeniman::all();
        return view('Content.seniman.list-komentar-seniman', $data);
    }

    public function create($id)
    {
        $data['table_karya'] = table_karya::find($id);
        return view('Content.seniman.input-komentar', $data);
    }

    public function edit($id)
    {
        $data['komentar_seniman'] = KomentarSeniman::find($id);
        return view('Content.seniman.edit-komentar', $data);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        KomentarSeniman::find($id)->update($input);
        return redirect('seniman/list-komentar-seniman');
    }

    public function delete($id, Request $request)
    {
        $seniman = KomentarSeniman::find($id);
        $seniman->delete($request->all());
        return redirect('seniman/list-komentar-seniman');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        KomentarSeniman::create($input);
        return redirect('seniman/list-komentar-seniman');
    }  

}
