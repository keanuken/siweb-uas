<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KomentarUser;
use App\Models\table_karya;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KomentarUserController extends Controller
{
    //
    public function index()
    {
        // $data['komentar_user'] = KomentarUser::all();
        $username = Auth::user()->name;
        $data['komentar_user']  = 
        DB::table('komentar_user AS ku')
        ->join('users as u', 'ku.id_user', '=', 'u.id')
        ->select('ku.*', 'u.name as name')
        ->where('u.name', '=', $username)
        ->get();
        return view('Content.user.list-komentar-user', $data);
    }

    public function create($id)
    {
        $data['table_karya'] = table_karya::find($id);
        return view('Content.user.input-komentar', $data);
    }

    public function edit($id)
    {
        $data['komentar_user'] = KomentarUser::find($id);
        return view('Content.user.edit-komentar', $data);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        KomentarUser::find($id)->update($input);
        return redirect('user/list-komentar-user');
    }

    public function delete($id, Request $request)
    {
        $user = KomentarUser::find($id);
        $user->delete($request->all());
        return redirect('user/list-komentar-user');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        KomentarUser::create($input);
        return redirect('user/list-komentar-user');
    }
}
