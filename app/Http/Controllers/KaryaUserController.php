<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\table_karya;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\View;

class KaryaUserController extends Controller
{
    //
    public function index(){
        $data['table_karya'] = table_karya::all();
        return view('content.user.index',$data);
    }
}
