<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\table_karya;
use App\Models\table_kategori;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Symfony\Contracts\Service\Attribute\Required;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ImageUploadController extends Controller
{
    public function index()
    {

        $username = Auth::user()->name;
        $data['table_karya']  =
            DB::table('table_karya AS tk')
            ->join('users as u', 'tk.id_seniman', '=', 'u.id')
            ->select('tk.*', 'u.name as name')
            ->where('u.name', '=', $username)
            ->get();

        return view('content.seniman.index', $data);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'id_seniman' => 'required',
            'nama_karya' => 'required',
            'deskripsi' => 'required',
            'wujud' => 'required',
            'image' => 'required',
        ]);


        $input = $request->all();

        if ($request->hasFile('image')) {
            $destination_path = 'assets/upload/image';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        table_karya::create($input);
        return redirect('seniman');
    }

    public function edit($id)
    {
        $data['table_karya'] = table_karya::find($id);
        return view('content.seniman.input-karya-edit', $data);
    }

    public function delete($id, Request $request)
    {
        $data = table_karya::find($id);
        $data->delete($request->all());
        return redirect('/seniman');
    }

    public function showKategori()
    {
        $data['table_kategori'] = table_kategori::all();
        return view('content.seniman.input-karya', $data);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        table_karya::find($id)->update($input);
        return redirect('seniman');
    }
}
