<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        $username = Auth::user()->name;
        // $data['users']  = 
        //         DB::table('users AS u')
        //         ->join('table_karya as tk', 'tk.id_seniman', '=', 'u.id')
        //         ->select('u.*', 'tk.image as image')
        //         ->groupby('u.id')
        //         ->get();
        $data['users'] = DB::table('users AS u')
                ->join('table_karya as tk', 'tk.id_seniman', '=', 'u.id')
                ->select('u.*', DB::raw('MAX(tk.image) AS image'))
                ->groupBy('u.id')
                ->get();

        return view('content.user.index', $data);
    }

    public function showusername()
    {
        if (Auth::check()) {
            $userName = Auth::user()->name;
            return " " . $userName;
        }
        
        return "Tidak ada pengguna yang login saat ini.";
    }
}
