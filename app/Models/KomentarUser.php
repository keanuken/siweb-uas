<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KomentarUser extends Model
{
    use HasFactory;
    protected $table = "komentar_user";
    protected $fillable = ['id_komentar', 'id_user', 'id_karya', 'komentar', 'tanggal'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
