<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KomentarSeniman extends Model
{
    use HasFactory;
    protected $table = "komentar_seniman";
    protected $fillable = ['id_komentar', 'id_user', 'id_karya', 'komentar', 'tanggal'];
}
