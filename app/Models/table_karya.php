<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class table_karya extends Model
{
    use HasFactory;

    protected $table = 'table_karya';

    protected $fillable = [
        'id_seniman',
        'nama_karya',
        'deskripsi',
        'wujud',
        'image',
    ];

       
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
